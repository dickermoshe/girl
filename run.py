import os

from gpiozero.pins.pigpio import PiGPIOFactory
from gpiozero import MotionSensor

from pynput.keyboard import Key, Controller

RIP = '192.168.1.12'

def set_cwd_to_script_location():
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    
def minimize_all():
    keyboard = Controller()
    keyboard.press(Key.cmd)
    keyboard.press('m')
    keyboard.release(Key.cmd)
    keyboard.release('m')
    os.system('setvol.exe mute')

def main():
    set_cwd_to_script_location()
    factory = PiGPIOFactory(host=RIP)
    pir = MotionSensor(4,pin_factory=factory)
    while True:
        pir.wait_for_motion()
        minimize_all()
        pir.wait_for_no_motion()

if __name__ =='__main__':
    main()